// Slider Header
$(document).ready(function () {
  $('.slider-header').slick({
    autoplay: true,
    autoplaySpeed: 2500,
    dots: true
  });
});
// End Slider Header

// slider Content
$(document).ready(function () {
  $('.variable-width').slick({
    dots: true,
    infinite: true,
    autoplay: false,
    arrows: false,
    speed: 200,
    variableWidth: true
  });
});
// End slider Content


// Toggle Visibility Password
$(".toggle-password").click(function () {

  $(this).toggleClass("bi-eye bi-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});
// End Toggle Visibility Password


// Disabling form submissions if there are invalid fields
(function () {
  'use strict'

  // Bootstrap validation
  var forms = document.querySelectorAll('.needs-validation')

  // Loop over them and prevent submission
  Array.prototype.slice.call(forms)
    .forEach(function (form) {
      form.addEventListener('submit', function (event) {
        if (!form.checkValidity()) {
          event.preventDefault()
          event.stopPropagation()
        }

        form.classList.add('was-validated')
      }, false)
    })
})()
// End Disabling form submissions if there are invalid fields


// OTP Password
function getCodeBoxElement(index) {
  return document.getElementById('codeBox' + index);
}

function onKeyUpEvent(index, event) {
  const eventCode = event.which || event.keyCode;
  if (getCodeBoxElement(index).value.length === 1) {
    if (index !== 4) {
      getCodeBoxElement(index + 1).focus();
    } else {
      getCodeBoxElement(index).blur();

      let otp = document.querySelector('#codeBox1').value
      otp += document.querySelector('#codeBox2').value
      otp += document.querySelector('#codeBox3').value
      otp += document.querySelector('#codeBox4').value

      document.querySelector('input[name=otp]').value = otp;
      document.querySelector('form').submit();
    }
  }
  if (eventCode === 8 && index !== 1) {
    getCodeBoxElement(index - 1).focus();
  }
}

function onFocusEvent(index) {
  for (item = 1; item < index; item++) {
    const currentElement = getCodeBoxElement(item);
    if (!currentElement.value) {
      currentElement.focus();
      break;
    }
  }
}

function sendOtp(_token) {
  const token = $('input[name=token]').val();

  $.ajax({
    url: '/password/otp/' + token,
    type: 'POST',
    data: {
      _token
    },
    success: function (res) {
      if (res.status) {
        var myModal = new bootstrap.Modal(document.getElementById('modalVerifikasiTerkirim'));
        myModal.show();
      }
    },
  })
}
// End OTP Password

// Alert Success
var alertPlaceholder = document.getElementById('liveAlertPlaceholder')
var alertTrigger = document.getElementById('liveAlertBtn')

function alert(message, type) {
  var wrapper = document.createElement('div')
  wrapper.innerHTML = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">' + message + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'

  alertPlaceholder.append(wrapper)
}

if (alertTrigger) {
  alertTrigger.addEventListener('click', function () {
    alert('Profile berhasil diubah')
  })
}
//End Alert Success

// Maximal input nama produk
$(document).ready(function () {
  $('#inputNamaProduk').keyup(function () {
    var len = this.value.length;
    if (len >= 70) {
      this.value = this.value.substring(0, 70);
    }
    $('#hitungNamaMax').text(70 - len);
    $('#hitungNamaMin').text(len);
  });
});
//End Maximal input nama produk

// Maximal textarea deskripsi produk
$(document).ready(function () {
  $('#inputDeskripsiProduk').keyup(function () {
    var len = this.value.length;
    if (len >= 200) {
      this.value = this.value.substring(0, 200);
    }
    $('#hitungDeskripsiMax').text(200 - len);
    $('#hitungDeskripsiMin').text(len);
  });
});
//End Maximal textarea deskripsi produk



// Search Location
// getting all required elements
const searchWrapper = document.querySelector(".search-input");
const inputBox = searchWrapper.querySelector("input");
const suggBox = searchWrapper.querySelector(".autocom-box");
const icon = searchWrapper.querySelector(".icon");
let linkTag = searchWrapper.querySelector("a");
let webLink;

// if user press any key and release
inputBox.onkeyup = (e) => {
  let userData = e.target.value; //user enetered data
  let emptyArray = [];
  if (userData) {
    icon.onclick = () => {
      webLink = `https://www.google.com/search?q=${userData}`;
      linkTag.setAttribute("href", webLink);
      linkTag.click();
    }
    emptyArray = suggestions.filter((data) => {
      //filtering array value and user characters to lowercase and return only those words which are start with user enetered chars
      return data.toLocaleLowerCase().startsWith(userData.toLocaleLowerCase());
    });
    emptyArray = emptyArray.map((data) => {
      // passing return data inside li tag
      return data = `<li>${data}</li>`;
    });
    searchWrapper.classList.add("active"); //show autocomplete box
    showSuggestions(emptyArray);
    let allList = suggBox.querySelectorAll("li");
    for (let i = 0; i < allList.length; i++) {
      //adding onclick attribute in all li tag
      allList[i].setAttribute("onclick", "select(this)");
    }
  } else {
    searchWrapper.classList.remove("active"); //hide autocomplete box
  }
}

function select(element) {
  let selectData = element.textContent;
  inputBox.value = selectData;
  icon.onclick = () => {
    webLink = `https://www.google.com/search?q=${selectData}`;
    linkTag.setAttribute("href", webLink);
    linkTag.click();
  }
  searchWrapper.classList.remove("active");
}

function showSuggestions(list) {
  let listData;
  if (!list.length) {
    userValue = inputBox.value;
    listData = `<li>${userValue}</li>`;
  } else {
    listData = list.join('');
  }
  suggBox.innerHTML = listData;
}



let suggestions = [
  "Keramik",
  "Keramik Merah",
  "Keramik Putih",
  "Keramik Biru",
  "Steker Lampu",
  "Wallpaper dinding",
  "Pasir",

];
// End Search Location


// Readmore and Readless
function readMore(productDetail) {
  let hiddenCaption = document.querySelector(`.header-read-more[header-kode="${productDetail}"] .hiddenCaption`);
  let moreText = document.querySelector(`.header-read-more[header-kode="${productDetail}"] .read-less`);
  let btnText = document.querySelector(`.header-read-more[header-kode="${productDetail}"] .read-more`);
  let readSorted = document.querySelector(`.header-read-more[header-kode="${productDetail}"] .read-sorted`);

  if (hiddenCaption.style.display === "none") {
    hiddenCaption.style.display = "inline";
    btnText.textContent = "Baca Selengkapnya";
    moreText.style.display = "none";
    readSorted.style.display = 'inline';
  } else {
    hiddenCaption.style.display = "none";
    btnText.textContent = "Sembunyikan";
    moreText.style.display = "inline";
    readSorted.style.display = 'none';
  }
}

function readMore(promoDetail) {
  let hiddenCaption = document.querySelector(`.header-read-more-promo[header-kode="${promoDetail}"] .hiddenCaption`);
  let moreText = document.querySelector(`.header-read-more-promo[header-kode="${promoDetail}"] .read-less`);
  let btnText = document.querySelector(`.header-read-more-promo[header-kode="${promoDetail}"] .read-more`);
  let readSorted = document.querySelector(`.header-read-more-promo[header-kode="${promoDetail}"] .read-sorted`);

  if (hiddenCaption.style.display === "none") {
    hiddenCaption.style.display = "inline";
    btnText.textContent = "Selengkapnya";
    moreText.style.display = "none";
    readSorted.style.display = 'inline';
  } else {
    hiddenCaption.style.display = "none";
    btnText.textContent = "Sembunyikan";
    moreText.style.display = "inline";
    readSorted.style.display = 'none';
  }
}
// End Readmore and Readless


// Button plus minus
function wcqib_refresh_quantity_increments() {
  jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function (a, b) {
    var c = jQuery(b);
    c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
  })
}
String.prototype.getDecimals || (String.prototype.getDecimals = function () {
  var a = this,
    b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
  return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
}), jQuery(document).ready(function () {
  wcqib_refresh_quantity_increments()
}), jQuery(document).on("updated_wc_div", function () {
  wcqib_refresh_quantity_increments()
}), jQuery(document).on("click", ".plus, .minus", function () {
  var a = jQuery(this).closest(".quantity").find(".qty"),
    b = parseFloat(a.val()),
    c = parseFloat(a.attr("max")),
    d = parseFloat(a.attr("min")),
    e = a.attr("step");
  b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
});
// End button plus minus